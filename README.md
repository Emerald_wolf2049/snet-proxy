# snet-proxy

# Steps To Run

## Build docker image

``` 
    docker build -t snet-proxy .

```
## Start service

``` 
    docker run -it snet-proxy
```

# Troubleshooting

## Change infura key endpoint when it reaches maximum call 

When infura API reaches maximum number of calls, snet client call returns  "Error: 429 Client Error: Too Many Requests for url:endpoint". Changing the endpoint by creating new one fixes the error.

To change the endpoint:

``` 
    Register
    https://infura.io/register

    Login
    https://infura.io/login

    Create project

    Copy endpoint for specific network

    Set endpoint values on config.py
```