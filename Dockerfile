FROM ubuntu:18.04
ARG MODE
ENV MODE=$MODE
RUN apt-get update && \
    apt-get install -y \
        apt-utils \
        curl \
        wget \
        vim \
        git \
        zip \
        libudev-dev \
        libusb-1.0.0-dev \
        libxml2-dev \
        libxslt-dev \
        libjpeg-dev \
        zlib1g-dev \
        libpng-dev \
        python3.7 \
        python3-pip
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
RUN apt-get install python3.7-dev -y 
RUN pip3 install snet.sdk

RUN apt-get update

RUN pip3 install snet-cli #( Install snet-cli)
RUN mkdir -p /var/log/supervisor

RUN mkdir p /root/.snet && echo  \
"[network.ropsten] \n\
default_eth_rpc_endpoint = https://ropsten.infura.io/v3/03d8d698a7c444ce970ef743486bf608 \n\
default_gas_price = medium \n\
\n\
[ipfs]\n\
default_ipfs_endpoint = http://ipfs.singularitynet.io:80 \n\
\n\
[session]\n\
network = ropsten" > /root/.snet/config


COPY requirements.txt /
RUN pip3 install -r /requirements.txt

COPY . /SNET-PROXY
WORKDIR /SNET-PROXY


RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8

EXPOSE 6000

CMD ["python3", "app.py"]
